import json
from unittest import TestCase
from unittest.mock import patch

from fxq.gcp.pub_sub import TopicPublisher


class TopicPublisherTest(TestCase):

    @patch('fxq.gcp.pub_sub.PublisherClient')
    def setUp(self, MockPublisherClient) -> None:
        self.topic_publisher: TopicPublisher = TopicPublisher("A_PROJECT", "A_TOPIC")

    def test_topic_publisher_initializes_with_correct_topic_path_arguments(self, ):
        """Test Constructor initializes topic_path with correct parameters"""
        self.topic_publisher._publisher.topic_path.assert_called_once_with("A_PROJECT", "A_TOPIC")

    def test_publish_calls_gcloud_publisher_publish_with_encoded_data(self):
        """Test when calling publish the data is passed to the gcloud publsuher and is published as encoded json"""
        self.topic_publisher._publisher.publish.return_value.exception.return_value = None

        dataf = {
            "hello": "world"
        }

        self.topic_publisher.publish(dataf)
        self.topic_publisher._publisher.publish.assert_called_once_with(
            self.topic_publisher._topic_path,
            data=json.dumps(dataf).encode("utf-8")
        )

    def test_publish_adds_additional_kwargs_as_attributes(self):
        """Test when calling publish with additional kwargs they are passed to the publish to be used as attributes"""
        self.topic_publisher._publisher.publish.return_value.exception.return_value = None

        timestamp_example = "1585856225448"
        dataf = {
            "hello": "world"
        }

        self.topic_publisher.publish(dataf, provider="fxcm", last_updated=timestamp_example)
        self.topic_publisher._publisher.publish.assert_called_once_with(
            self.topic_publisher._topic_path,
            data=json.dumps(dataf).encode("utf-8"),
            provider="fxcm", last_updated=timestamp_example
        )

    def test_publish_adds_additional_dict_attributes_as_message_attributes(self):
        """Test when calling publish with additional dict attributes they are passed to the publish
        to be used as message attributes"""
        self.topic_publisher._publisher.publish.return_value.exception.return_value = None

        timestamp_example = "1585856225448"
        dataf = {
            "hello": "world"
        }

        attributes = {
            "provider": "fxcm",
            "last_updated": timestamp_example
        }

        self.topic_publisher.publish(dataf, attributes)
        self.topic_publisher._publisher.publish.assert_called_once_with(
            self.topic_publisher._topic_path,
            data=json.dumps(dataf).encode("utf-8"),
            provider="fxcm", last_updated=timestamp_example
        )

    def test_publish_adds_additional_dict_attributes_along_with_kwargs_as_message_attributes(self):
        """Test when calling publish with additional dict attributes and kwargs they are passed to the publish
        to be used as message attributes"""
        self.topic_publisher._publisher.publish.return_value.exception.return_value = None

        timestamp_example = "1585856225448"
        dataf = {
            "hello": "world"
        }

        attributes = {
            "provider": "fxcm",
            "last_updated": timestamp_example
        }

        self.topic_publisher.publish(dataf, attributes, source="website")
        self.topic_publisher._publisher.publish.assert_called_once_with(
            self.topic_publisher._topic_path,
            data=json.dumps(dataf).encode("utf-8"),
            provider="fxcm", last_updated=timestamp_example, source="website"
        )

    def test_publish_returns_result_of_calling_future_result(self):
        """Test when calling publish the result of the call to future is returned"""
        self.topic_publisher._publisher.publish.return_value.exception.return_value = None

        dataf = {
            "hello": "world"
        }

        ret_val = self.topic_publisher.publish(dataf)
        self.assertEqual(self.topic_publisher._publisher.publish.return_value.result.return_value, ret_val)
